﻿namespace RomListFilter.Model {

    public class DatGameRom {

		public string Name { get; set; }

		public int Size { get; set; }

		public string Crc { get; set; }

		public string Md5 { get; set; }

		public string Sha1 { get; set; }

		public string Status { get; set; }
	}
}
