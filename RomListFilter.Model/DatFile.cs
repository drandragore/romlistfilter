﻿using System.Collections.Generic;

namespace RomListFilter.Model {

    public class DatFile {

		private IList<DatGame> games;

		public DatFile() {
			games = new List<DatGame>();
		}

		public DatHeader Header { get; set; }

		public IList<DatGame> Games {
			get { return games; }
		}
    }
}
