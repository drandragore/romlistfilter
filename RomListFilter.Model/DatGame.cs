﻿using System.Collections.Generic;

namespace RomListFilter.Model {

    public class DatGame {

		private IList<DatGameRelease> releases;

		public DatGame() {
			releases = new List<DatGameRelease>();
		}

		public string Name { get; set; }

		public string Description { get; set; }

		public string CloneOf { get; set; }

		public IList<DatGameRelease> Releases {
			get { return releases; }
		}
	}
}
