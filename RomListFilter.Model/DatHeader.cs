﻿namespace RomListFilter.Model {

    public class DatHeader {

		public string Name { get; set; }

		public string Description { get; set; }

		public string Version { get; set; }

		public string Date { get; set; }

		public string Author { get; set; }

		public string Url { get; set; }
	}
}
